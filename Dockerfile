FROM envoyproxy/envoy:v1.14-latest

WORKDIR /home

RUN apt-get update
RUN apt-get install autoconf automake libtool curl make g++ unzip git -y
RUN mkdir protobuf
RUN cd protobuf
RUN git init
RUN git remote add origin https://github.com/google/protobuf.git
RUN git pull origin master
RUN git submodule update --init --recursive
RUN ./autogen.sh
RUN ./configure
RUN make
RUN make install
RUN ldconfig
RUN protoc
